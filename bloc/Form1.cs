﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bloc
{
    public partial class Form1 : Form
    {
        public String filename;

        public Form1()
        {
            InitializeComponent();
        }

        private void cortarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Cut();
        }

        private void copiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Copy();
        }

        private void pegarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Paste();
        }

        private void nuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Declare open as a new OpenFileDailog
            OpenFileDialog open = new OpenFileDialog();
            //Set the Filename of the OpenFileDailog to nothing
            open.FileName = "";
            //Declare filter as a String equal to our wanted OpenFileDialog Filter
            String filter = "Text Files|*.txt|All Files|*.*";
            //Set the OpenFileDialog's Filter to filter
            open.Filter = filter;
            //Set the title of the OpenFileDialog to Open
            open.Title = "Open";
            //Show the OpenFileDialog
            if (open.ShowDialog(this) == DialogResult.OK)
            {
                //Declare filename as a String equal to the OpenFileDialog's FileName
                filename = open.FileName;
                //Make the richTextBox1's Text equal to all of the text in the OpenFileDialog's FileName(filename)
                richTextBox1.Text = System.IO.File.ReadAllText(filename);
            }
        }

        private void guardarcomoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Declare save as a new SaveFileDailog
            SaveFileDialog save = new SaveFileDialog();
            //Declare filter as a String equal to our wanted SaveFileDialog Filter
            String filter = "Text Files|*.txt|All Files|*.*";
            //Set the SaveFileDialog's Filter to filter
            save.Filter = filter;
            //Set the title of the SaveFileDialog to Save
            save.Title = "Save";
            //Show the SaveFileDialog
            if (save.ShowDialog(this) == DialogResult.OK) {
                //Declare filename as a String equal to the SaveFileDialog's FileName
                filename = save.FileName;
                //Write all of the text in txtBox to the specified file
                System.IO.File.WriteAllText(filename, richTextBox1.Text);
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void deshacerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Undo();
        }

        private void rehacerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Redo();
        }

        private void seleccionartodoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectAll();
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(filename)) {
                System.IO.File.WriteAllText(filename, richTextBox1.Text);
            } else {
                //Declare save as a new SaveFileDailog
                SaveFileDialog save = new SaveFileDialog();
                //Declare filter as a String equal to our wanted SaveFileDialog Filter
                String filter = "Text Files|*.txt|All Files|*.*";
                //Set the SaveFileDialog's Filter to filter
                save.Filter = filter;
                //Set the title of the SaveFileDialog to Save
                save.Title = "Save";
                //Show the SaveFileDialog
                if (save.ShowDialog(this) == DialogResult.OK) {
                    //Declare filename as a String equal to the SaveFileDialog's FileName
                    filename = save.FileName;
                    //Write all of the text in txtBox to the specified file
                    System.IO.File.WriteAllText(filename, richTextBox1.Text);
                }
            }
        }
    }
}
